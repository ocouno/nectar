<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/thanks/', function()
{
	/*
	$message ="Hello you are shit arse bollox twat cunt swear word filtration test";
	$result = Filter::filter($message,'love');
	if( strlen($result) < strlen($message)){
		$message = 'Stop swearing!!';
	} else {
		$message = $result;
	}
	$data = Input::all();
	var_dump($name);
	*/

	$count 			= Messages::count() * 10;
	$messages 		= Messages::orderBy('id', 'DESC')->paginate(100);
	$url 			= Config::get('app.url');

	$data = (object) array(
		'count' => $count,
		'messages' => $messages,
		'url' => $url
	);

	return View::make('index')->with('data', $data);
});


// Show message
Route::get('/thanks/message/{id}/', function($id)
{

	$count 			= Messages::count() * 10;
	$featured 		= Messages::find($id);
	$messages 		= Messages::orderBy('id', 'DESC')->paginate(100);
	$url 			= Config::get('app.url');

	$data = (object) array(
		'count' => $count,
		'messages' => $messages,
		'featured' => $featured,
		'url' => $url
	);
	
	return View::make('featured')->with('data', $data);
})->where('id', '[0-9]+');

// Show message
Route::get('/thanks/message/view/{id}/', function($id)
{

	$count 			= Messages::count() * 10;
	$featured 		= Messages::find($id);
	$messages 		= Messages::orderBy('id', 'DESC')->paginate(100);
	$url 			= Config::get('app.url');

	$data = (object) array(
		'count' => $count,
		'messages' => $messages,
		'featured' => $featured,
		'url' => $url
	);
	
	return View::make('view')->with('data', $data);
})->where('id', '[0-9]+');



// Show message
Route::get('/thanks/image/{id}/', function($id)
{


	$featured 		= Messages::find($id);
	$url 			= Config::get('app.url');

	$data = (object) array(
		'featured' => $featured,
		'url' => $url
	);
	
	return View::make('image')->with('data', $data);
})->where('id', '[0-9]+');


// Post message
Route::get('/thanks/message/post/', function()
{
	$url		= Config::get('app.url');
	$data		= Input::all();

	if(Request::ajax()){

		$validator = Validator::make(
		    
		    array(
		        'message' => urldecode(strip_tags($data['message']) ),
		        'recipient' => urldecode(strip_tags($data['recipient'] ) ),
		        'sender' => urldecode(strip_tags($data['sender'] ) ),
		        // 'email' => urldecode(strip_tags($data['email'] ) ),
		        'icon' => urldecode(strip_tags($data['icon'] ) )
		    ),
		    array(
		        'message' => 'required|max:57',
		        'recipient' => 'required|max:14',
		        'sender' => 'required|max:14',
		        // 'email' => 'required|email',
		        'icon' => 'required|max:57|in:cat,diamond,flower,glass,heart,like,lips,ok,present,stars,sun'
		    )
		);

		if ($validator->fails()) {
		    return $validator->messages();
	    } else {
	        $m = new Messages;
	        $m->message 	= Filter::filter( urldecode(strip_tags( $data['message'] ) ),'nectar');
	        $m->recipient 	= Filter::filter( urldecode(strip_tags( $data['recipient'] ) ),'nectar');
	        $m->sender 		= Filter::filter( urldecode(strip_tags( $data['sender'] ) ),'nectar');
	        $m->icon 		= Filter::filter( urldecode(strip_tags( $data['icon'] ) ),'nectar');

	        //if success
	        if($m->save()){

	        	$insertedId = $m->id;
	        	if(is_numeric($insertedId)){
					$path = realpath('./');
					$genimage = '/usr/local/bin/wkhtmltoimage --crop-w 344 --crop-w 344 '.escapeshellcmd($url).'/thanks/image/'.escapeshellcmd($insertedId).' '.escapeshellcmd($path).'/mimg/'.escapeshellcmd($insertedId).'.jpg';
	        		exec($genimage,$foo,$bar);
	        	}

	            return $insertedId;
	        } else {
	        	return 0;
	        }
	    }
    }
});

Route::get('/thanks/message/share/', function()
{
	$url		= Config::get('app.url');
	$data		= Input::all();

	if(Request::ajax()){

		$validator = Validator::make(
		    
		    array(
		        'message_id' => urldecode(strip_tags($data['message_id'] ) ),
		        'recipient' => urldecode(strip_tags($data['recipient'] ) ),
		        'sender' => urldecode(strip_tags($data['sender'] ) ),
		        
		    ),
		    array(
		        'message_id' => 'required|numeric',
		        'recipient' => 'required|email',
		        'sender' => 'required',
		    )
		);

		if ($validator->fails()) {
		    return $validator->messages();
	    } else {

	    	$message = Messages::find( $data['message_id'] );

	        $data = array(
				'sender' => urldecode(strip_tags($data['sender'] ) ),
				'recipient' => urldecode(strip_tags($data['recipient'] ) ),
				'recipient_name' => $message->recipient,
				'message_id' => urldecode( strip_tags( $data['message_id'] ) ),
				'url' => $url
			);

			Mail::send('email.share', $data, function($message) use ($data)
			{
			    $message->from( 'thanks@nectar.com', 'nectar.com' );
			    $message->to( $data['recipient'] );
			    $message->subject( 'Thanks a million!' );

			});
			return 1;

	    }
    }
});

Route::get('/thanks/message/form/', function()
{
	$url		= Config::get('app.url');
	$data		= Input::all();
	return View::make('form');
});