<p>Dear <?php echo $recipient_name; ?></p>
<p>Good news! <?php echo $sender; ?> just sent you a "thanks a million" message with Nectar. </p>
<p><a href="<?php echo $url; ?>/thanks/message/view/<?php echo $message_id; ?>">Click here to see the message</a></p>
<p>Once you’ve read their "thanks a million" message, make sure you post your own message too - and brighten the day of someone you know.</p>
<p><a href="<?php echo $url; ?>/thanks/message/view/<?php echo $message_id; ?>">Say thanks a million now &gt;</a></p>