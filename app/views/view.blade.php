
<!doctype html>  

<!--[if IEMobile 7 ]> <html lang="en-GB" prefix="og: http://ogp.me/ns#"class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie9"> <![endif]-->
<!--[if (gte IE 10)|(gt IEMobile 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!--[if (IE)]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>nectar.com</title>
	<meta name="meta-description" content="">
	<link rel="stylesheet" href="/library/css/style.css?ver=1.0" type="text/css" media="all" />
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144-precomposed.png">
	<link rel="shortcut icon" href="/favicon.png" />
	
	<meta property="og:url" content="{{{ $data->url }}}thanks/message/view/{{{ $data->featured->id }}}"/>
	<meta property="og:image" content="{{{ $data->url }}}/mimg/{{{ $data->featured->id }}}.jpg"/>
	<meta property="og:site_name" content="nectar.com"/>
	<meta property="og:title" content="Good things start with a thank you – so I’ve just said thanks a million!"/>
	<meta property="og:description" content="Check out my message – and post your own 'thank you' to someone special...  #thanksamillion"/>
	<script type='text/javascript' src='/library/js/other_components/modernizr.js'></script>
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-5352042-1']);
	_gaq.push(['_setDomainName', 'nectar.com']);
	_gaq.push(['_setAllowLinker', true]);
	_gaq.push(['_trackPageview']);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>

	<!-- COREMETRICS TEST ENVIRONMENT -->
	<script type="text/javascript" src="//libs.de.coremetrics.com/eluminate.js"></script>
	<script type="text/javascript">
	cmSetClientID("80470000|Summer Hub", false, "testdata.de.coremetrics.com", "nectar.com");
	</script>

	<!-- COREMETRICS PRODUCTION ENVIRONMENT -->
	<script type="text/javascript" src="//libs.de.coremetrics.com/eluminate.js"></script>
	<script type="text/javascript">
	cmSetClientID("50470000|Summer Hub", true, "data.de.coremetrics.com", "nectar.com");
	</script>

</head>
<body>
	<div id="pagearea" class="pagearea clearfix" >
		<header id="header" class="header clearfix">
			<div class="fullwrap">
				<h1><a href="http://nectar.com">nectar.com</a></h1>
				<ul class="social">
					<li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fpo.st%2FRxd6sM" class="head facebook">Facebook</a></li>
					<li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fpo.st%2FWe37bq&text=Nectar%20|%20Collect%20and%20Spend%20Nectar%20points&via=nectar" class="head twitter">twitter</a></li>
					<li><a href="http://www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.nectar.com%2FNectarHome.nectar%23LIFOqcgJ4wLmJQfY.32&description=Nectar%20|%20Collect%20and%20Spend%20Nectar%20points&media=http://www.nectar.com/contents/images/site/logoNectar-new.png" class="head pintrest">pintrest</a></li>
					<li><a href="https://plus.google.com/share?url=http%3A%2F%2Fwww.nectar.com%2FNectarHome.nectar%23q6IrGeM7w3AQrOkV.30&t=Nectar%20|%20Collect%20and%20Spend%20Nectar%20points" class="head google">google</a></li>
				</ul>
			</div>
		</header>
		<section class="contentarea">
			<img src="/library/img/bg-content.jpg" alt="Content background" class="bg-content">

			<div class="contentheader clearfix">
				<div class="container_12">
					<div class="grid_3">
						<a href="/"><img src="/library/img/logo.png" alt="Nectar - Thanks a million"></a>
					</div>
					<div class="grid_6">
						<img src="/library/img/strap.png" alt="Nectar - Thanks a million">
						<p>It only takes a moment to say THANK YOU and brighten the day for friends, family and colleagues. <a href="#popup" class="showpopup">Add your message below, share it and then encourage others to do the same!</a></p>
						
					</div>
					<div class="grid_3">
						<p class="screenonly">&nbsp;</p>
						<p><a href="#popup" class="showpopup"><img src="/library/img/post.png" alt="Nectar - Thanks a million"></a></p>
						<p class="mobileonly">&lt; SWIPE &gt;</p>
					</div>
				</div>
			</div>
			<div class="content cleaarfix">
				<div class="itemsarea cleaarfix">
				<?php 
				$colours =  array('purple','yellow','green','pink');
				foreach ($data->messages as $key=>$message)
				{

					
					if( !isset($data->featured) && $key % 7 === 0 ){
						$itemsize = 'large';
					} else {
						$itemsize = null;
					}


					$colour = $colours[rand(0,3)];
				    ?>
					<?php
				    if(isset($data->featured)){
				    	if($key === 0){
				    ?>
					<div class="item large mobileonly {{{ $colour }}}">
						<div class="inside clearfix">
							<img src="/library/svg/icon-usr-{{{ $data->featured->icon }}}.png" alt="{{{ $data->featured->icon }}}" class="icon">
							<h2>Thanks a Million</h2>
							<h3>{{{ $data->featured->recipient }}}</h3>
							<p>{{{ $data->featured->message }}}</p>
							<p>{{{ $data->featured->sender }}}</p>
							<div class="shareblock {{{ $colour }}}">
								<div class="wrap">
								<p><strong>Love your friend’s ‘thank you’ message? Make sure you pots your own message to brighten the day on someone you know!</strong></p>
								</div>
							</div>
						</div>
						<img src="/library/svg/corner-{{{ $colour }}}.png" alt="Corner" class="corner">
					</div>
				    <?php
				    	}
				    }
				    ?>
					<div class="item fade {{{ $colour }}} {{{ $itemsize }}}">
						<div class="inside clearfix">
							<img src="/library/svg/icon-usr-{{{ $message->icon }}}.png" alt="{{{ $message->icon }}}" class="icon">
							<h2>Thanks a Million</h2>
							<h3>{{{ $message->recipient }}}</h3>
							<p>{{{ $message->message }}}</p>
							<p>{{{ $message->sender }}}</p>
						</div>
						<img src="/library/svg/corner-{{{ $colour }}}.png" alt="Corner" class="corner">
					</div>
				    <?php
				    if(isset($data->featured)){
				    	if($key === 11){
				    ?>
					<div class="item large screenonly {{{ $colour }}}">
						<div class="inside clearfix">
							<img src="/library/svg/icon-usr-{{{ $data->featured->icon }}}.png" alt="{{{ $data->featured->icon }}}" class="icon">
							<h2>Thanks a Million</h2>
							<h3>{{{ $data->featured->recipient }}}</h3>
							<p>{{{ $data->featured->message }}}</p>
							<p>{{{ $data->featured->sender }}}</p>

							<div class="shareblock {{{ $colour }}}">
								<div class="wrap">
								<p><strong>Love your friend’s ‘thank you’ message? Make sure you pots your own message to brighten the day on someone you know!</strong></p>
								</div>
							</div>
						</div>
						<img src="/library/svg/corner-{{{ $colour }}}.png" alt="Corner" class="corner">
					</div>
				    <?php
				    	}
				    }
				    ?>
				<?php
				}
				?>
				</div>
				<div id="page_nav">
					<ul class="pagination">
						<li><a href="{{{ $data->url }}}/?page=2">2</a>
					</li>
				</div>
			</div>


		</section>
		<?php
			$totalizer_percentage = $data->count / ( 1000000 / 100 );
			$totalizer_iconration = 78 / 100;
			$totalizer_iconposition = $totalizer_percentage * $totalizer_iconration;
		?>
		<!-- Column pages -->
		<footer id="footer" class="panel footer clearfix">
			<div class="container_12">
				<div class="grid_2">
					<p><a href="http://www.nectar.com/spend/oxfam-unwrapped.points"><img src="/library/img/logo-oxfam.png" alt="oxfam logo small"></a></p>
				</div>
				<div class="grid_9">
					<p>Nectar is saying thanks a million in another way too – by donating 1 million Nectar points to Oxfam. The work Oxfam does makes a big difference to people in need worldwide, by providing essentials like clean water and mosquito nets. <a href="http://www.nectar.com/spend/oxfam-unwrapped.points">Learn more and help Oxfam</a></p>
					<p class="text-right"><small><a href="/terms/index.html">Terms & Conditions</a></small></p>
				</div>
			</div>
		</footer>
		<!-- News -->


	</div><!-- eo page -->

	<div id="popup" class="popup">
		<div class="form">
			<div class="itemwrap">
				<a href="#popup" class="close">x</a>

				<form id="postform">
					<div class="item large flexy yellow">
						<div class="inside replaceme">
							<img src="/library/svg/icon-usr-heart.png" alt="heart" class="icon">
							<h2>Thanks a Million</h2>
							<div class="itemform">
								<div class="err"></div>
								<input type="hidden" id="icon" name="icon" value="heart">
								<p><input type="text" name="recipient" id="recipient" maxlength="14" placeholder="Who do you want to say thank you to?"></p>
								<p><textarea name="message" id="message" cols="30" rows="3" placeholder="What is your thank you message? (Max 57 characters)"></textarea></p>
								<p class="counter">Remaining characters: (<span id="charNum">57</span>)</p>
								<p><input type="text" name="sender" id="sender" maxlength="14" placeholder="From (Your name)"></p>
							</div>
						</div>						
					</div>


					<div class="itembottom">
						<div class="iconselection">
							<div class="cell">Choose a badge:</div>
							<div class="cell"><ul class="icondropdown">
								<li class="dropdown"><a href="#heart" class="heart">heart</a>
									<ul class="dropdownlist">
										<li><a href="#cat" class="cat">cat</a></li>
										<li><a href="#diamond" class="diamond">diamond</a></li>
										<li><a href="#flower" class="flower">flower</a></li>
										<li><a href="#glass" class="glass">glass</a></li>
										<li><a href="#heart" class="heart">heart</a></li>
										<li><a href="#like" class="like">like</a></li>
										<li><a href="#lips" class="lips">lips</a></li>
										<li><a href="#ok" class="ok">ok</a></li>
										<li><a href="#present" class="present">present</a></li>
										<li><a href="#stars" class="stars">stars</a></li>
										<li><a href="#sun" class="sun">sun</a></li>
									</ul>
								</li>
							</ul>
							</div>
						</div>
						<br>
						<p><button class="button">Post your thank you</button></p>
						<p><a href="/terms/index.html">Terms and conditions</a></p>
					</div>

				</form>

			</div>
		</div>
	</div>
	<div class="sharewithfriend">
				
	</div>

	<script type='text/javascript' src='/library/js/min/plugins-min.js'></script>
	<script type='text/javascript' src='/library/js/min/main-min.js'></script>
	<script>
		jQuery(document).ready(function($) {

			$('.sharelink').click(function(){
				$( ".sharewithfriend" ).load( "/thanks/message/form?v=3",function(){
				    $(".sharewithfriend .close").click(function(e) {
				        $('.sharewithfriend').removeClass('active');
				    });
					$("#shareform .button").click(function(e){
						e.preventDefault();
						var recipient   = $(".recipentemail").val();
					    var message_id  = '{{{ $data->featured->id }}}';
					    var sender      = '{{{ $data->featured->sender }}}';
					    var dataString = 'message_id='+message_id+'&recipient='+recipient+'&sender='+sender; 
		    
					    $.ajax({
					        type: "GET",
					        url : "{{{ $data->url }}}thanks/message/share",
					        data : dataString,
					        success : function(data){
					        	
					            if (data){
					            	$( ".sharewithfriend .wrap" ).html('<p>Your share request has been sent.</p>');
					            } else {
					            	
					            	var error = [];

									if (data['recipient']) { error.push(data['recipient']); }
									
									var error_message = error.join('<br>');

									$('.err').html(error_message);
					            }
					            
					        },
					        error : function(data){
					        	console.log('error');
					        }
					    },"json");
					});
				});
				$( ".sharewithfriend" ).addClass('active');

			});
			


			$("#postform").submit(function(e){
		        e.preventDefault();
		        var recipient   = $("#recipient").val();
		        var message     = $("#message").val();
		        var sender      = $("#sender").val();
		        var email       = $("#email").val();
		        var icon        = $("#icon").val();

		        var dataString = 'recipient='+recipient+'&message='+message+'&sender='+sender+'&email='+email+'&icon='+icon; 
		        
		        $.ajax({
		            type: "GET",
		            url : "{{{ $data->url }}}thanks/message/post",
		            data : dataString,
		            success : function(data){
		            	
		                if (data > 0){
		                	window.location.replace("/thanks/message/" + data);
		                } else {
		                	
		                	var error = [];
		                	
		                	if (data['message']) { error.push(data['message']); }
							if (data['recipient']) { error.push(data['recipient']); }
							if (data['sender']) { error.push(data['sender']); }
							if (data['email']) { error.push(data['email']); }
							if (data['icon']) { error.push(data['icon']); }
							
							var error_message = error.join('<br>');

							$('.err').html(error_message);
		                }
		                
		            },
		            error : function(data){
		            	console.log('error');
		            }
		        },"json");
		    });
		});
	</script>
<script type="text/javascript">
cmCreatePageviewTag("Thanks Hub: " + document.location.pathname + "", "Thanks Hub: Hub pages", "", "", "");
</script>
</body>
</html>