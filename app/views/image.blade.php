
<!doctype html>  

<!--[if IEMobile 7 ]> <html lang="en-GB" prefix="og: http://ogp.me/ns#"class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js ie9"> <![endif]-->
<!--[if (gte IE 10)|(gt IEMobile 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-GB" prefix="og: http://ogp.me/ns#" class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!--[if (IE)]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>nectar.com</title>
	<meta name="meta-description" content="">
	<link rel="stylesheet" href="/library/css/style.css?ver=1.0" type="text/css" media="all" />
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144-precomposed.png">
	<link rel="shortcut icon" href="/favicon.png" />
	
	<meta property="og:title" content="nectar.com"/>
	<meta property="og:url" content="{{{ $data->url }}}/"/>
	<meta property="og:image" content="{{{ $data->url }}}/apple-touch-icon-144x144-precomposed.png"/>
	<meta property="og:site_name" content="nectar.com"/>
	<meta property="fb:admins" content=""/>
	<meta property="og:description" content=""/>

	<script type='text/javascript' src='/library/js/other_components/modernizr.js'></script>
</head>
<body style="background: #fff; padding: 10px;">
<?php
	$colours =  array('purple','yellow','green','pink');
	$colour = $colours[rand(0,3)];
?>
<div class="crop">
	<div class="item svgfont large {{{ $colour }}}">
		<div class="inside" style="height: 250px;">
			<img src="/library/svg/icon-usr-{{{ $data->featured->icon }}}.png" width="80" height="80" alt="{{{ $data->featured->icon }}}" class="icon">
			<h2>Thanks a Million</h2>
			<h3>{{{ $data->featured->recipient }}}</h3>
			<p>{{{ $data->featured->message }}}</p>
			<p>{{{ $data->featured->sender }}}</p>
		</div>						
	</div>
</div>
</body>
</html>