
//PLAYS YOUTUBE VIDEO ON CLICK
$('.video img').click(function(){
    video = '<iframe width="100%" height="288" src="'+ $(this).attr('data-video') +'"></iframe>';
    $(this).replaceWith(video);
});

// IDEAS CAROUSEL
$(document).ready(function(){

    $('.carouselContent').owlCarousel({
        autoplay: true,
        loop: true,
        autoplayTimeout: 100,
        autoplayHoverPause: true,
        smartSpeed: 3000,
        center: false,
        margin: 0,
        nav: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            880:{
                items:3
            }
        }
    });

    // DUCK FALLING ANIMATION
    /*
    $('.duck').on('change', function() {
        var	elem = $('.duck'),
            effect = $(this).val();

        elem.removeClass().addClass('animating bm-remove').addClass(effect);

        elem.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            elem.removeClass(effect).removeClass('animating');
        });
    });
    */
});

// TERMS ACCORDION
$(function() {
    $('#accordion').accordion({
        heightStyle: "content"
    });
});