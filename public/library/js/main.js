jQuery(document).ready(function($) {
    
    //Ensures there will be no 'console is undefined' errors
    window.console = window.console || (function(){
        var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(){};
        return c;
    })();

    /////////////////////////////////////////////////////
    // Isotope setup
    /////////////////////////////////////////////////////
    
    if(window.innerWidth > 665){
        if(( window.innerHeight - 373) > 430){
            $('.itemsarea').height( ( window.innerHeight - 373) + 'px');    
        } else {
            $('.itemsarea').height( '480px');    
        }
        
        $('.itemsarea').height(  + 'px');
        $('.itemsarea.home').animate({ left: "-1000px", easing: "linear" }, 14000 );
    } else {
        $('.itemsarea').height( '480px');    
    }


    $('.itemsarea').isotope({
        layoutMode: 'masonryHorizontal',
        itemSelector: '.item',
        masonryHorizontal: {
          rowHeight: 113
        }
    });

    /////////////////////////////////////////////////////
    // Item state change and isotore re - layout
    /////////////////////////////////////////////////////
    
    $(".itemsarea").click(function(e) {
        if (e.target.id === ".enlarge" || $(e.target).parents(".enlarge").size()) { 
            $('.itemsarea').stop();

            if(window.innerWidth < 665){
                
                $(".item").removeClass('large');

                $(e.target).parents(".item").addClass('large');

                var itleft = $(e.target).parents(".item").position().left;
                var moveto = "-"+(itleft - 10)+"px";

                if($(e.target).parents(".item").hasClass('large')){
                    $('.itemsarea').animate({"left":moveto},800);                    
                }

            } else {
                $(e.target).parents(".item").toggleClass('large');    
            }
            $('.itemsarea').isotope().isotope('layout');

        }
    });
    $(".showpopup").click(function(e) {
        e.preventDefault();
        $('.popup').addClass('active');
    });
    $(".close").click(function(e) {
        e.preventDefault();
        $('.popup').removeClass('active');
        $('.sharewithfriend').removeClass('active');
    });

    function countChar(val) {
        var len = val.value.length;
        if (len >= 57) {
            val.value = val.value.substring(0, 57);
        } else {
            $('#charNum').text(56 - len);
        }
    }
    $('#message').keyup(function() {
        countChar(this);
    });


    /////////////////////////////////////////////////////
    // Infinity scroll setup
    /////////////////////////////////////////////////////

    $('.itemsarea').infinitescroll({
            loading: {
                finished: '',
                finishedMsg: '',
                img: '',
                msg: '',
                msgText: ''
            },
            navSelector  : '.pagination',    // selector for the paged navigation 
            nextSelector : '.pagination li:last-child a',  // selector for the NEXT link (to page 2)
            itemSelector : '.itemsarea .item'
        },
        // get new items and append to .itemsarea
        function( newElements ) {
            $('.itemsarea').isotope( 'appended', $( newElements ) ).isotope('layout'); 
        }
    );





    function animateMessages(link,speed){
        
        speed = speed ? speed : '800' ;

        var scrollto;
        var leftoffset = $('.itemsarea').position().left;
        var rightoffset =  $(window).width() - ( $('.itemsarea').offset().left + $('.itemsarea').width() );
        var scrollpixels = ($(window).width() > 665) ? ( $(window).width() / 2 ) : 320 ;
        var nextlimit = '-'+ ($(window).width() / 2 );
        var leftposition = '-' + ($('.itemsarea').width() - $(window).width());

        if(link === '#next' ){
            if(rightoffset < nextlimit){
                scrollto = leftoffset - scrollpixels;
            } else {
                scrollto = leftposition;
            }
        } else if ( link === '#back' ) {
            if(leftoffset < nextlimit){
                scrollto = leftoffset + scrollpixels;// left;    
            } else {
                console.log('0');
                scrollto = 20;
            }
            
        } else {
            scrollto = null;
        }

        if (scrollto){
            $('.itemsarea').animate({"left" : scrollto+"px"}, speed);
        }


        var new_window_width = $(window).width();
        var newposition_left = $('.itemsarea').position().left;
        var newdiv_width = $('.itemsarea').width();
        
        if ( ( newdiv_width + newposition_left ) - new_window_width < new_window_width && link === '#next' ){
            $('.itemsarea').infinitescroll('retrieve');
        }
    }


    if( $('.slidernav').length ) {
        $('.slidernav').click(function(e){
            e.preventDefault();
            $('.itemsarea').stop();
            animateMessages($(this).attr('href'));
        });
    }

    if(window.innerWidth < 665){
        $(".content").swipe({
            swipe:function(event, direction) {
                var move;
                if (direction === 'right') {
                    move = '#back';
                    animateMessages(move,'200');
                } else if (direction === 'left') {
                    move = '#next';
                    animateMessages(move,'200');
                }
            },
            threshold:100,
            maxTimeThreshold:500,
            allowPageScroll: 'vertical'
        });
    }

    $('.icondropdown').on('click','.dropdown > a ',function(e){
        e.preventDefault();
        $('.dropdownlist').toggleClass('active');
    });
    $('.icondropdown').on('click','.dropdownlist > li > a ',function(e){
        e.preventDefault();
        var thisclass = $(this).attr('class');
        $('.dropdownlist').toggleClass('active');
        $('#icon').val(thisclass);
        $('.replaceme img.icon').attr('src','/library/svg/icon-usr-'+thisclass+'.png');
        $('.dropdown > a').attr('class',thisclass);
    });
    
    // $.ajax({
    //     type: "POST",
    //     dataType: "json",
    //     data: {test : worked},
    //     url: 'ajax/getDude.php',
    //     success: function(data) {
    //     alert(data);
    //     }
    // });

     // var MEASUREMENTS_ID = 'measurements'; // abstracted-out for convenience in renaming
     // $("body").append('<div id="'+MEASUREMENTS_ID+'"></div>');
     // $("#"+MEASUREMENTS_ID).css({
     //     'position': 'fixed',
     //     'bottom': '0',
     //     'right': '0',
     //     'background-color': 'black',
     //     'color': 'white',
     //     'padding': '5px',
     //     'font-size': '10px',
     //     'opacity': '0.4'
     // });
     // function getDimensions(){
     //     return $(window).width() + ' (' + $(document).width() + ') x ' + $(window).height() + ' (' + $(document).height() + ') inner ('+window.innerHeight+')';
     // }
     // $("#"+MEASUREMENTS_ID).text(getDimensions());
     // $(window).on("resize", function(){
     //     $("#"+MEASUREMENTS_ID).text(getDimensions());
     // });


});
